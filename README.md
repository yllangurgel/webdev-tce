# API Projeto Desenvolvimento WEB
Este projeto é referente à API utilizada para o desenvolvimento do projeto TabManager da disciplina de Desenvolvimento WEB.

## Passos para rodar a aplicação
- Adicionar a ConnectionString no arquivo appsettings.Development.json e no appsettings.json
```json
    "ConnectionString": { "NetCoreUserConnect":<sua string de conexão aqui> }
```
- Rodar a migração para criação do banco e das tabelas no SQL Server com o comando abaixo: 
```shell
    dotnet ef database update
``` 
- Para rodar o servidor de desenvolvimento com hot reload rode o comando abaixo:
```shell
    dotnet watch run
```
A aplicação irá rodar na url "https://localhost:5001".