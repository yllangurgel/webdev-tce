using System.ComponentModel.DataAnnotations;

using webdev_project.Domain.Models;

namespace webdev_project.Domain.Entities
{
    public class WorkspaceDTO
    {
        [Required]
        public int WorkspaceId { get; set; }

        [Required]
        [MinLength(4)]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public string AccessLevel { get; set; }
    }

    public class PostWorkspaceDTO
    {
        [Required]
        [MinLength(4)]
        [MaxLength(100)]
        public string Name { get; set; }
    }

    public class PutNameWorkspaceDTO
    {
        [Required]
        [MinLength(4)]
        [MaxLength(100)]
        public string Name { get; set; }
    }
}