using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace webdev_project.Domain.DTO
{
    public class WorkspaceItemDTO
    {
        /// <summary>
        /// ID do workspace.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkspaceId { get; set; }

        /// <summary>
        /// Nome desse workspace.
        /// </summary>
        [Required]
        [MinLength(4)]
        [MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// Nivel de acesso do usuário à este workspace.
        /// Os valores possiveis são Owner, ReadWriteAccess, ReadOnly.
        /// </summary>
        [Required]
        public string AccessLevel { get; set; }
        // Tambem tera uma url que aponta pro endpoint REST onde é possível
        // obter os recursos (grupos de abas) que pertencem à este Workspace
    }
}
