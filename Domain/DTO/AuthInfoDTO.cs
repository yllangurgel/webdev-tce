


namespace webdev_project.Domain.DTO
{
    public class AuthInfoDTO
    {
        public JwtTokenDTO TokenInfo { get; set; }
        public UserInfoDTO UserInfo { get; set; }
    }
}
