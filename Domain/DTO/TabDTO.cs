using System.ComponentModel.DataAnnotations;

namespace webdev_project.Domain.DTO
{
    public class TabDTO
    {
        [Required]
        public int TabId { get; set; }

        [Required]
        [MaxLength(256)]
        public string Link { get; set; }

        [Required]
        public int ResourceId { get; set; }
    }

    public class PostTabDTO
    {
        [Required]
        [MaxLength(256)]
        public string Link { get; set; }

        [Required]
        public int ResourceId { get; set; }

        [Required]
        public int WorkspaceId { get; set; }
    }

    public class PutLinkTabDTO
    {
        [Required]
        [MaxLength(256)]
        public string Link { get; set; }
    }

    public class PutResourceIdTabDTO
    {
        [Required]
        public int ResourceId { get; set; }

        [Required]
        public int WorkspaceId { get; set; }
    }
}