using System;
using System.Collections.Generic;

namespace webdev_project.Domain.Models
{
    /// <summary>
    /// Um tipo de resultado que permite fazer pattern matching
    /// com um tipo generico.
    /// </summary>
    /// <example>
    /// AuthResult result = new Success<int>(10);
    /// var value = result switch {
    ///    Success<int> s => s.Data,
    ///    Error e => -1
    /// };
    ///
    /// Assert.IsEqual(10, value);
    /// </example>
    public abstract class AuthResult {}

    public class Success<T> : AuthResult {

        public Success(T data)
        {
            Data = data;
        }

        public T Data { get; } 
    }

    public class Error : AuthResult {
        private List<Exception> _errors = new List<Exception>();

        public Error(ICollection<Exception> errors)
        {
            _errors.AddRange(errors);
        }

        public IEnumerable<Exception> Errors {
            get { return _errors; }
        }
    }
}
