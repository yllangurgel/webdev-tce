


namespace webdev_project.Domain.Models
{
    public class JwtSettings
    {
        public string Issuer { get; set; }
        public string Key { get; set; }
    }
}
