using AutoMapper;

using webdev_project.Domain.Entities;
using webdev_project.Domain.DTO;

namespace webdev_project.Domain.Profiles
{
    public class JwtProfile : Profile
    {
        public JwtProfile()
        {
            CreateMap<RefreshToken, RefreshTokenDTO>();
        }
    }
}
