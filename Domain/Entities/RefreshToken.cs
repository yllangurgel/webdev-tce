using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace webdev_project.Domain.Entities
{
    /// <summary>
    /// Token utilizada para obter novas tokens JWT sem precisar
    /// informar as credenciais do usuário.
    /// </summary>
    [Table("RefreshToken", Schema = "Data")]
    public class RefreshToken
    {
        public int RefreshTokenId { get; set; }
        public string Token { get; set; }
        public DateTime Expires { get; set; }
        public DateTime Created { get; set; }
        public string CreatedByIp { get; set; }
        public DateTime? Revoked { get; set; }
        public string RevokedByIp { get; set; }

        public bool IsExpired => DateTime.UtcNow >= Expires;
        public bool IsActive => Revoked == null && !IsExpired;
    }
}
