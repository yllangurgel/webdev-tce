
using webdev_project.Domain.DTO;
using webdev_project.Domain.Entities;

namespace webdev_project.Services
{
    public interface IJwtService
    {
        public JwtTokenDTO GetTokenForUser(User user, string ipAddress);
        public JwtTokenDTO RefreshToken(string token, string ipAddress);
    }
}
