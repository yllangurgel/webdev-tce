using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;

using AutoMapper;

using webdev_project.Infra;
using webdev_project.Domain.Entities;
using webdev_project.Domain.DTO;
using webdev_project.Domain.Models;

namespace webdev_project.Services
{
    public class WorkspaceService : IWorkspaceService
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<WorkspaceService> _log;

        public WorkspaceService(
            AppDbContext context,
            IMapper mapper,
            ILogger<WorkspaceService> log
        )
        {
            _context = context;
            _mapper = mapper;
            _log = log;
        }

        public async Task<Workspace> GetWorkspaceById(int workspaceId)
        {
            var ws = await _context.Workspaces
                .AsNoTracking()
                .Include(ws => ws.WorkspaceConfigs)
                .SingleOrDefaultAsync(ws => ws.WorkspaceId == workspaceId);
            if (ws == null) throw new NullReferenceException($"Não existe um Workspace com a WorkspaceId:{workspaceId}.");
            return ws;
        }

        public async Task<IEnumerable<ResourceDTO>> GetResourcesByWorkspaceId(int workspaceId)
        {
            var resourceList = await _context.Resources.Where(rs => rs.WorkspaceId == workspaceId).ToListAsync();
            return resourceList.Select(rs => _mapper.Map<ResourceDTO>(rs));
        }

        public async Task<WorkspaceDTO> CreateWorkspace(int userId, PostWorkspaceDTO workspaceDTO)
        {
            var ws = Workspace.CreateWithOwner(workspaceDTO.Name, userId);
            await _context.Workspaces.AddAsync(ws);
            await _context.SaveChangesAsync();
            return new WorkspaceDTO()
            {
                WorkspaceId = ws.WorkspaceId,
                Name = ws.Name,
                AccessLevel = AccessLevel.Owner.ToString("g")
            };
        }

        public async Task<WorkspaceDTO> UpdateWorkspaceName(int workspaceId, PutNameWorkspaceDTO workspaceDTO)
        {
            var ws = await _context.Workspaces.SingleOrDefaultAsync(ws => ws.WorkspaceId == workspaceId);
            if (ws == null) throw new NullReferenceException($"Não existe um Workspace com a WorkspaceId:{workspaceId}.");

            ws.Name = workspaceDTO.Name;
            await _context.SaveChangesAsync();
            return new WorkspaceDTO()
            {
                WorkspaceId = ws.WorkspaceId,
                Name = ws.Name,
                AccessLevel = AccessLevel.Owner.ToString("g")
            };
        }

        public async Task DeleteWorkspace(int workspaceId)
        {
            var ws = await _context.Workspaces.SingleOrDefaultAsync(ws => ws.WorkspaceId == workspaceId);
            if (ws == null) throw new NullReferenceException($"Não existe um Workspace com a WorkspaceId:{workspaceId}.");
            _context.Workspaces.Remove(ws);
            await _context.SaveChangesAsync();
        }

        public async Task<WorkspaceConfigDTO> CreateWorkspacePermission(PostWorkspaceConfigDTO workspaceConfigDTO)
        {
            var ws = await _context.Workspaces
                .Include(wsc => wsc.WorkspaceConfigs)
                .SingleOrDefaultAsync(ws => ws.WorkspaceId == workspaceConfigDTO.WorkspaceId);
            if (ws == null) throw new NullReferenceException($"Não existe um Workspace com a WorkspaceId:{workspaceConfigDTO.WorkspaceId}.");
            if (ws.HasMember(workspaceConfigDTO.UserId)) throw new ArgumentException($"Já existe uma configuração para UserId:{workspaceConfigDTO.UserId}");
            var wsc = _mapper.Map<WorkspaceConfig>(workspaceConfigDTO);
            ws.WorkspaceConfigs.Add(wsc);
            await _context.SaveChangesAsync();
            return _mapper.Map<WorkspaceConfigDTO>(wsc);
        }

        public async Task<WorkspaceConfigDTO> UpdateWorkspacePermission(int workspaceId, PutAcessLevelWorkspaceConfigDTO workspaceConfigDTO)
        {
            var ws = await _context.Workspaces
                .Include(wsc => wsc.WorkspaceConfigs)
                .SingleOrDefaultAsync(ws => ws.WorkspaceId == workspaceId);
            if (ws == null) throw new NullReferenceException($"Não existe um Workspace com a WorkspaceId:{workspaceId}.");
            if (!ws.HasMember(workspaceConfigDTO.UserId)) throw new ArgumentException($"Não existe uma configuração para UserId:{workspaceConfigDTO.UserId}");
            var wsc = ws.WorkspaceConfigs.SingleOrDefault(u => u.UserId == workspaceConfigDTO.UserId);
            wsc.AccessLevel = (AccessLevel)Enum.Parse(typeof(AccessLevel), workspaceConfigDTO.AccessLevel);
            await _context.SaveChangesAsync();
            return _mapper.Map<WorkspaceConfigDTO>(wsc);
        }

        public async Task DeleteWorkspacePermission(int workspaceId, DeleteWorkspaceConfigDTO workspaceConfigDTO)
        {
            var ws = await _context.Workspaces
                .Include(wsc => wsc.WorkspaceConfigs)
                .SingleOrDefaultAsync(ws => ws.WorkspaceId == workspaceId);
            if (ws == null) throw new NullReferenceException($"Não existe um Workspace com a WorkspaceId:{workspaceId}.");
            if (!ws.HasMember(workspaceConfigDTO.UserId)) throw new ArgumentException($"Não existe uma configuração para UserId:{workspaceConfigDTO.UserId}");
            var wsc = ws.WorkspaceConfigs.SingleOrDefault(u => u.UserId == workspaceConfigDTO.UserId);
            ws.WorkspaceConfigs.Remove(wsc);
            await _context.SaveChangesAsync();
        }
    }
}
