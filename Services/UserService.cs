using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

using webdev_project.Infra;
using webdev_project.Domain.Entities;
using webdev_project.Domain.Models;

namespace webdev_project.Services
{
    /// <inheritdoc/>
    public class UserService : IUserService
    {
        private readonly AppDbContext _context;
        private readonly ILogger<UserService> _log;

        public UserService(
            AppDbContext context,
            ILogger<UserService> log
        )
        {
            _context = context;
            _log = log;
        }

        public async Task<User> GetUserById(int userId)
        {
            return await _context.Users
                .Include(u => u.RefreshTokens)
                .AsNoTracking()
                .SingleOrDefaultAsync(u => u.UserId == userId);
        }

        public async Task<IEnumerable<WorkspaceDTO>> GetWorkspacesByUserId(int userId, AccessLevel? accessLevel = null)
        {
            IQueryable<User> users = _context.Users;
            if (accessLevel != null)
            {
                users = users.Where(user => user.WorkspaceConfigs.All(config => config.AccessLevel == accessLevel));
            }

            var userWithWorkspaces = await users.Include(u => u.WorkspaceConfigs)
                .ThenInclude(config => config.Workspace)
                .Where(u => u.UserId == userId)
                .ToListAsync();

            return userWithWorkspaces.SelectMany(ws => {
                return ws.WorkspaceConfigs.Select(conf => new WorkspaceDTO()
                {
                    WorkspaceId = conf.Workspace.WorkspaceId,
                    Name = conf.Workspace.Name,
                    AccessLevel = conf.Workspace.GetAccesLevelForUser((int)userId).ToString("g")
                });
            });
        }

        // public async Task CreateWorkspaceForUser(int userId, Workspace workspace)
        // {
        //     var currentUser = await _context.Users.SingleAsync(u => u.UserId == userId);

        //     // Usuário inexistente (Não deve acontecer)
        //     if (currentUser == null)
        //     {
        //         _log.LogError($"Não foi possível obter workspace para o usuário com id: {userId}");
        //         throw new ApplicationException("Não foi possível criar workspace para o usuário atual. Id inválido.");
        //     }

        //     var newConfig = new WorkspaceConfig()
        //     {
        //         Member = currentUser,
        //         Workspace = workspace,
        //         AccessLevel = AccessLevel.Owner,
        //     };

        //     workspace.WorkspaceConfigs = new List<WorkspaceConfig>() { newConfig };
        //     await _context.Workspaces.AddAsync(workspace);

        //     await _context.SaveChangesAsync();
        // }

        // public async Task<Workspace> GivePermissionToUser(int receivingUserId, int workspaceId, AccessLevel accessLevelGranted)
        // {
        //     var workspace = await _context.Workspaces.Include(ws => ws.WorkspaceConfigs).SingleAsync(ws => ws.WorkspaceId == workspaceId);
        //     var workspaceConfig = new WorkspaceConfig()
        //     {
        //         UserId = receivingUserId,
        //         WorkspaceId = workspaceId,
        //         AccessLevel = accessLevelGranted
        //     };
        //     workspace.WorkspaceConfigs.Add(workspaceConfig);
        //     try
        //     {
        //         await _context.SaveChangesAsync();
        //     }
        //     catch (DbUpdateException)
        //     {
        //         return null;
        //     }
        //     return workspace;
        // }

    }
}
