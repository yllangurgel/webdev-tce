using System;
using System.Linq;
using System.Collections.Generic;

using webdev_project.Domain.Entities;
using webdev_project.Domain.Models;
using webdev_project.Infra;

namespace webdev_project.Services
{
    public class AuthService : IAuthService
    {
        private readonly AppDbContext _context;

        public AuthService(AppDbContext context)
        {
            _context = context;
        }

        public bool CheckPassword(User user, string password)
        {
            return user.Password == password;
        }

        public AuthResult CreateUser(User user, string password)
        {
            var errors = new List<Exception>();
            var existingUser = _context.Users
                .Where(u => u.UserName == user.UserName)
                .SingleOrDefault();

            if (existingUser != null)
            {
                errors.Add(new ArgumentException("Já existe um usuário com este nome."));
            }

            if (errors.Count != 0)
            {
                return new Error(errors);
            }

            user.Password = password;
            _context.Users.Add(user);
            _context.SaveChanges();
            return new Success<User>(user);
        }

        public User FindUserByName(string username)
        {
            return _context.Users
                .Where(u => u.UserName == username)
                .SingleOrDefault();
        }
    }
}
