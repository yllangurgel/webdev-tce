using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

using AutoMapper;

using webdev_project.Services;
using webdev_project.Domain.DTO;
using webdev_project.Domain.Entities;
using webdev_project.Domain.Models;

namespace webdev_project.Api.Controllers
{

    [Route("api/user/")]
    [Authorize]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly ILogger<UserController> _log;

        public UserController(
            IUserService userService,
            IMapper mapper,
            ILogger<UserController> log
        )
        {
            _userService = userService;
            _mapper = mapper;
            _log = log;
        }

        private int? GetUserIdFromClaims()
        {
            int? userId = null;
            try
            {
                var userIdClaim = User.Claims.First(c => c.Type == ClaimTypes.Name)?.Value;
                userId = int.Parse(userIdClaim);
            }
            catch (Exception e)
            {
                _log.LogError("Não foi possível obter o id do usuário das tokens JWT. Erro: {ErrorMessage}", e.Message);
            }
            return userId;
        }

        /// <summary>
        /// Obtem todas as refresh tokens emitidas para o usuário atual.
        /// </summary>
        /// <remarks>
        /// Iria ser utilizado para revogação de tokens, mas como o outro
        /// endpoint não foi implementado, não está sendo utilizado ainda.
        /// </remarks>
        [HttpGet("{id}/refresh-tokens")]
        [ProducesResponseType(typeof(IEnumerable<RefreshTokenDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetRefreshTokens([FromRoute] int id)
        {
            var user = await _userService.GetUserById(id);
            if (user == null)
            {
                return NotFound();
            }

            var refreshTokens = user.RefreshTokens.Select(token => _mapper.Map<RefreshTokenDTO>(token));
            return Ok(refreshTokens);
        }

        /// <summary>
        /// Obtem todos os workspaces que o usuário atual possui acesso.
        /// </summary>
        /// <remarks>
        /// O usuário atual é definido com base na token JWT.\
        /// Todos os workspaces que esse usuário é membro são retornados, incluindo diferentes tipos de acesso.
        /// </remarks>
        [HttpGet("workspaces")]
        [ProducesResponseType(typeof(IEnumerable<WorkspaceDTO>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetWorkspacesByUserId([FromQuery] string accessLevel)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido");
            }

            AccessLevel? accessLevelFilter = null;
            if (accessLevel != null)
            {
                try {
                    accessLevelFilter = (AccessLevel) Enum.Parse(typeof(AccessLevel), accessLevel);
                } catch (ArgumentException)
                {
                    return BadRequest("filtro de nivel de acesso inválido");
                }
            }

            var workspaces = await _userService.GetWorkspacesByUserId((int)userId, accessLevelFilter);
            return Ok(workspaces);
        }
    }
}
