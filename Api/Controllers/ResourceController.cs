using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

using webdev_project.Services;
using webdev_project.Domain.DTO;
using webdev_project.Domain.Entities;
using webdev_project.Domain.Models;

namespace webdev_project.Api.Controllers
{

    [Route("api/resource/")]
    [Authorize]
    [ApiController]
    public class ResourceController : Controller
    {
        private readonly IWorkspaceService _workspaceService;
        private readonly IResourceService _resourceService;
        private readonly IAuthorizationService _authService;
        private readonly ILogger<ResourceController> _log;

        public ResourceController(
            IWorkspaceService workspaceService,
            IResourceService resourceService,
            IAuthorizationService authService,
            ILogger<ResourceController> log
        )
        {
            _workspaceService = workspaceService;
            _resourceService = resourceService;
            _authService = authService;
            _log = log;
        }

        private int? GetUserIdFromClaims()
        {
            int? userId = null;
            try
            {
                var userIdClaim = User.Claims.First(c => c.Type == ClaimTypes.Name)?.Value;
                userId = int.Parse(userIdClaim);
            }
            catch (Exception)
            {
                _log.LogError("Não foi possível obter o id do usuário das tokens JWT.");
                return null;
            }
            return userId;
        }

        /// <summary>
        /// Retorna um Recurso pelo ResourceId.
        /// </summary>
        /// <remarks>
        /// Variáveis da Route: Seleciona o Recurso. \
        /// Variáveis do Body: Não possui. \
        /// Exemplo: \
        /// Route: GET /api/resource/2/workspace/1 \
        /// Body:
        /// </remarks>
        [HttpGet("{resourceId:int}/workspace/{workspaceId:int}")]
        [ProducesResponseType(typeof(ResourceDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetResourceById([FromRoute] int resourceId, [FromRoute] int workspaceId)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido");
            }
            var workspace = await _workspaceService.GetWorkspaceById(workspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{workspaceId} não foi encontrado.");
                return NotFound();
            }
            var authResult = await _authService.AuthorizeAsync(User, workspace, "CanViewWorkspace");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao ler o WorkspaceId:{workspaceId}");
                return Unauthorized();
            }

            try
            {
                var rs = await _resourceService.GetResourceById(resourceId, workspaceId);
                return Ok(rs);
            }
            catch (NullReferenceException)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Retorna todos as Tab vinculadas a esse Recurso.
        /// </summary>
        /// <remarks>
        /// Variáveis da Route: Seleciona o Recurso. \
        /// Variáveis do Body: Não possui. \
        /// Exemplo: \
        /// Route: GET /api/resource/2/workspace/1/tabs \
        /// Body:
        /// </remarks>
        [HttpGet("{resourceId:int}/workspace/{workspaceId:int}/tabs")]
        [ProducesResponseType(typeof(IEnumerable<TabDTO>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetTabsByResourceId([FromRoute] int resourceId, [FromRoute] int workspaceId)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido");
            }

            var workspace = await _workspaceService.GetWorkspaceById(workspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{workspaceId} não foi encontrado.");
                return NotFound();
            }

            var authResult = await _authService.AuthorizeAsync(User, workspace, "CanViewWorkspace");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao ler o ResourceId:{resourceId}");
                return Unauthorized();
            }

            try
            {
                var result = await _resourceService.GetTabsByResourceId(resourceId, workspaceId);
                return Ok(result);
            }
            catch (InvalidOperationException)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Cria um Recurso.
        /// </summary>
        /// <remarks>
        /// Variáveis da Route: \
        /// Variáveis do Body: Define os parâmetros do novo Recurso. \
        /// Exemplo: \
        /// Route: GET /api/resource \
        /// Body: {"Name":"Tutoriais 5", "WorkspaceId":1}
        /// </remarks>
        [HttpPost]
        [ProducesResponseType(typeof(ResourceDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> CreateResource([FromBody] PostResourceDTO resourceDTO)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido");
            }

            var workspace = await _workspaceService.GetWorkspaceById(resourceDTO.WorkspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{resourceDTO.WorkspaceId} não foi encontrado.");
                return NotFound();
            }

            var authResult = await _authService.AuthorizeAsync(User, workspace, "CanModifyWorkspace");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao criar um Recurso no WorkspaceId:{resourceDTO.WorkspaceId}");
                return Unauthorized();
            }

            var rs = await _resourceService.CreateResource(resourceDTO);
            return Ok(rs);
        }

        /// <summary>
        /// Edita o nome do Recurso.
        /// </summary>
        /// <remarks>
        /// Variáveis da Route: Seleciona o Recurso. \
        /// Variáveis do Body: Define o novo nome do Recurso. \
        /// Exemplo: \
        /// Route: PUT /api/resource/2/workspace/1/name \
        /// Body: {"Name": "Tutoriais Pentaho"}
        /// </remarks>
        [HttpPut("{resourceId:int}/workspace/{workspaceId:int}/name")]
        [ProducesResponseType(typeof(ResourceDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> UpdateResourceName([FromRoute] int resourceId, [FromRoute] int workspaceId, [FromBody] PutNameResourceDTO resourceDTO)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido");
            }

            var workspace = await _workspaceService.GetWorkspaceById(workspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{workspaceId} não foi encontrado.");
                return NotFound();
            }

            var authResult = await _authService.AuthorizeAsync(User, workspace, "CanModifyWorkspace");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao editar o ResourceId:{resourceId}.");
                return Unauthorized();
            }

            try
            {
                var rs = await _resourceService.UpdateResourceName(resourceId, resourceDTO);
                return Ok(rs);
            }
            catch (NullReferenceException)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Edita o WorkspaceId do Recurso.
        /// </summary>
        /// <remarks>
        /// Variáveis da Route: Seleciona o Recurso. \
        /// Variáveis do Body: Define o novo WorkspaceId do Recurso. \
        /// Exemplo: \
        /// Route: PUT /api/resource/2/workspace/1/workspaceid \
        /// Body: {"WorkspaceId": 3}
        /// </remarks>
        [HttpPut("{resourceId:int}/workspace/{workspaceId:int}/workspaceid")]
        [ProducesResponseType(typeof(ResourceDTO), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> UpdateResourceWorkspaceId([FromRoute] int resourceId, [FromRoute] int workspaceId, [FromBody] PutWorkspaceIdResourceDTO resourceDTO)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido.");
            }
            var workspace = await _workspaceService.GetWorkspaceById(workspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{workspaceId} não foi encontrado.");
                return NotFound();
            }
            var authResult = await _authService.AuthorizeAsync(User, workspace, "CanModifyWorkspace");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao editar o ResourceId:{resourceId}.");
                return Unauthorized();
            }
            workspace = await _workspaceService.GetWorkspaceById(resourceDTO.WorkspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{resourceDTO.WorkspaceId} não foi encontrado.");
                return NotFound();
            }
            authResult = await _authService.AuthorizeAsync(User, workspace, "CanModifyWorkspace");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao editar o ResourceId:{resourceId}.");
                return Unauthorized();
            }

            try
            {
                var rs = await _resourceService.UpdateResourceWorkspaceId(resourceId, workspaceId, resourceDTO);
                return Ok(rs);
            }
            catch (NullReferenceException)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Deleta um Recurso.
        /// </summary>
        /// <remarks>
        /// Variáveis da Route: Seleciona o Recurso. \
        /// Variáveis do Body: \
        /// Exemplo: \
        /// Route: DELETE /api/resource/2/workspace/1 \
        /// Body:
        /// </remarks>
        [HttpDelete("{resourceId:int}/workspace/{workspaceId:int}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteResource([FromRoute] int resourceId, [FromRoute] int workspaceId)
        {
            var userId = GetUserIdFromClaims();
            if (userId == null)
            {
                return BadRequest("Token JWT Inválido");
            }

            var workspace = await _workspaceService.GetWorkspaceById(workspaceId);
            if (workspace == null)
            {
                _log.LogWarning($"WorkspaceId:{workspaceId} não foi encontrado.");
                return NotFound();
            }

            var authResult = await _authService.AuthorizeAsync(User, workspace, "CanModifyWorkspace");
            if (!authResult.Succeeded)
            {
                _log.LogInformation($"Autorização falhou para UserId:{userId} ao deletar o WorkspaceId:{workspaceId}");
                return Unauthorized();
            }

            try
            {
                await _resourceService.DeleteResource(resourceId);
                return NoContent();
            }
            catch (NullReferenceException)
            {
                return BadRequest();
            }
        }

    }
}