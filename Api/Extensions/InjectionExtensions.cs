using System;
using System.IO;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;

using webdev_project.Infra;
using webdev_project.Api.Filters;

namespace webdev_project.Api.Extensions
{
    public static class InjectionExtensions
    {
        public static void AddRepository<T, R>(this IServiceCollection services, Func<T, DbSet<R>> getDbSet)
            where T : DbContext
            where R : class
        {
            services.AddScoped<IRepository<R>, Repository<R>>(provider =>
            {
                var dbContext = provider.GetService<T>();
                return new Repository<R>(dbContext, getDbSet(dbContext));
            });
        }

        public static void AddSwaggerConfiguration(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Webdev TCE API",
                    Version = "v1"
                });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using bearer scheme.",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                c.OperationFilter<SecurityRequirementsOperationFilter>();

                var rootPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                var xmlDocsFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlDocsPath = $"{rootPath}/{xmlDocsFilename}";
                c.IncludeXmlComments(xmlDocsPath);
            });
        }
    }
}
