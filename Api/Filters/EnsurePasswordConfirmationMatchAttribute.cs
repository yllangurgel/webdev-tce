using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

using webdev_project.Domain.DTO;

namespace webdev_project.Api.Filters
{
    public class EnsurePasswordConfirmationMatchAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var createUserDto = context.ActionArguments["userCreateDto"] as UserCreateDTO;
            if (createUserDto.Password != createUserDto.PasswordConfirmation)
            {
                context.ModelState.AddModelError("PasswordConfirmation", "Password confirmation doesn't match");

                context.Result = new BadRequestObjectResult(context.ModelState);
            }
        }
    }
}
