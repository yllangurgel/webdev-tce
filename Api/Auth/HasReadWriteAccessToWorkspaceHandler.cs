using System;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

using webdev_project.Domain.Entities;
using webdev_project.Domain.Models;

namespace webdev_project.Api.Auth
{
    public class HasReadWriteAccessToWorkspaceHandler
        : AuthorizationHandler<CanModifyWorkspaceContentRequirement, Workspace>
    {
        private readonly ILogger<HasReadWriteAccessToWorkspaceHandler> _log;

        public HasReadWriteAccessToWorkspaceHandler(
            ILogger<HasReadWriteAccessToWorkspaceHandler> log
        )
        {
            _log = log;
        }

        protected override Task HandleRequirementAsync(
            AuthorizationHandlerContext context,
            CanModifyWorkspaceContentRequirement requirement,
            Workspace workspace
        )
        {
            var userIdClaim = context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value;
            if (userIdClaim == null)
            {
                _log.LogWarning("Não foi possível obter ID do usuário pelas claims");
                return Task.FromResult(0);
            }

            try
            {
                var userId = int.Parse(userIdClaim);

                var config = workspace.GetConfigForUser(userId);
                if (config != null && (config.AccessLevel == AccessLevel.ReadWriteAccess || config.AccessLevel == AccessLevel.Owner))
                {
                    context.Succeed(requirement);
                }
            }
            catch (Exception e)
            {
                _log.LogError(
                    @"Aconteceu um erro ao tentar autorizar acesso ao workspace {WorkspaceId}
                    com permissões de leitura e escrita. Erro: {ErrorMessage}", workspace.WorkspaceId, e.Message);
            }
            return Task.FromResult(0);
        }
    }
}
