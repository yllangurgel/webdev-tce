using Microsoft.AspNetCore.Authorization;

namespace webdev_project.Api.Auth
{
    public class HasReadAccessRequirement : IAuthorizationRequirement {}
}
