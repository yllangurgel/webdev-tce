using System;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

using webdev_project.Domain.Entities;

namespace webdev_project.Api.Auth
{
    public class IsMemberOfWorkspaceHandler : AuthorizationHandler<HasReadAccessRequirement, Workspace>
    {
        private readonly ILogger<IsMemberOfWorkspaceHandler> _log;

        public IsMemberOfWorkspaceHandler(ILogger<IsMemberOfWorkspaceHandler> log)
        {
            _log = log;
        }

        protected override Task HandleRequirementAsync(
            AuthorizationHandlerContext context,
            HasReadAccessRequirement requirement,
            Workspace workspace
        ) {
            var userIdClaim = context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value;
            if (userIdClaim == null) 
            {
                _log.LogInformation("Não foi possível obter id do usuário das claims");
                return Task.FromResult(0);
            }

            try {
                var userId = int.Parse(userIdClaim);
                if (workspace.HasMember(userId))
                {
                    context.Succeed(requirement);
                }
            } catch (Exception) {
                _log.LogWarning(
                    "Erro ao tentar autorizar acesso para leitura do workspace {WorkspaceId}", 
                    workspace.WorkspaceId
                );
                return Task.FromResult(0);
            }
            return Task.FromResult(0);
        }
    }
}
