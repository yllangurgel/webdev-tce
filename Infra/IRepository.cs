using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Query;

namespace webdev_project.Infra
{
    /// <summary>
    /// Implementa uma classe de repositorio similar à implementada no TCE, porém com
    /// a lógica pertencente aos repositórios e unit of work removidas devido 
    /// ao escopo do projeto ser menor (e com tempo bem mais curto para desenvolvimento).
    /// </summary>
    public interface IRepository<T>
    {
        /// <summary>
        /// Adiciona o objeto <paramref name="obj"/> no banco de dados.
        /// </summary>
        /// <param name="obj">o objeto a ser adicionado.</param>
        T Add(T obj);

        /// <summary>
        /// Faz update do objeto <paramref name="obj"/> no banco de dados.
        /// </summary>
        /// <param name="obj">o objeto a ser adicionado.</param>
        T Update(T obj);

        /// <summary>
        ///    Remove todos os objetos após serem filtrados pelo predicado.
        ///    Caso nenhum predicado seja passado, todos os objetos serão removidos.
        /// </summary>
        /// <param name="predicate">Predicado que define quais os objetos a serem removidos</param>
        void DeleteAll(Expression<Func<T, bool>> predicate = null);

        /// <summary>
        /// Deleta o objeto <paramref name="obj"> do banco de dados.
        /// </summary>
        /// <exception cref="System.ArgumentException">Obtido ao passar um parâmetro nulo para a função.</exception>
        void Delete(T entity);

        /// <summary>
        /// Obtem um conjunto de elementos que estejam de acordo com o 
        /// predicado <paramref name="predicate">,
        /// ordenados pela ordem definida por <paramref name="orderBy"/> 
        /// e com dados de outras tabelas (i.e. joins) obtidos por 
        /// meio de <paramref name="include">.
        /// </summary>
        IEnumerable<T> GetAll(
            Expression<Func<T, bool>> predicate = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null
        );

        /// <summary>
        /// Retorna o primeiro objeto encontrado com as características
        /// especificadas em <paramref name="predicate">.
        /// </summary>
        /// <param name="predicate">Predicado que irá selecionar o objeto no DB</param>
        /// <returns>O objeto definido pelo predicate ou nulo caso não seja encotrado.</returns>
        public T Single(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// Persiste as modificações no banco de dados.
        /// </summary>
        public void SaveChanges();
    }
}
