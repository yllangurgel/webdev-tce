using Microsoft.EntityFrameworkCore;

using webdev_project.Domain.Entities;

namespace webdev_project.Infra
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Workspace> Workspaces { get; set; }

        public DbSet<Resource> Resources { get; set; }
        public DbSet<Tab> Tabs { get; set; }
    }
}
